package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	//Stiamo utilizzando il line coverage
	
	
	
	@Test
	public void cellShouldBeAlive() throws Exception {

		Cell c = new Cell(0,0,true);
		assertTrue(c.isAlive());
					
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnX() throws Exception {

		new Cell(-1,0,true);
							
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnY() throws Exception {

		new Cell(0,-1,true);
							
	}
	
	@Test
	public void cellShouldBeDead() throws Exception {

		Cell c = new Cell(0,0,true);
		c.setAlive(false);
		
		assertFalse(c.isAlive());
					
	}
	
	@Test
	public void cellShouldReturnX() throws Exception {

		Cell c = new Cell(5,0,true);
				
		assertEquals(5, c.getX());
					
	}
	
	@Test
	public void cellShouldReturnY() throws Exception {

		Cell c = new Cell(0,5,true);
				
		assertEquals(5, c.getY());
					
	}
	
	@Test
	public void cellShouldHaveAliveNeighbors() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertEquals(3, cells[1][1].getNumberOfAliveNeighbords());
			
	}
	
	@Test
	public void aliveCellWith3ShouldSurvive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());
	}
	@Test
	public void aliveCellWith2NShouldSurvive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,true);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellShouldNotSurvive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void deadCellShouldNotSurvive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellWithMoreThen3NShouldDie() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willDie());
	}

	@Test
	public void aliveCellWithLessThen2NShouldDie() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,false);
		cells[0][2] = new Cell(0,2,false);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willDie());
	}
	@Test
	public void aliveCellShouldNotDie() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellShouldNotDie() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,true);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willDie());
	}
	
	@Test
	public void deadCellShouldRevive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertTrue(cells[1][1].willRevive());
	}
	
	@Test
	public void deadCellShouldNotRevive() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,true);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,true);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willRevive());
	}
	
	
	@Test
	public void deadCellShouldNotRevive2() throws Exception {

		Cell[][] cells = new Cell[3][3];
		cells[0][0] = new Cell(0,0,false);
		cells[0][1] = new Cell(0,1,true);
		cells[0][2] = new Cell(0,2,true);
		
		cells[1][0] = new Cell(1,0,false);
		cells[1][1] = new Cell(1,1,false);
		cells[1][2] = new Cell(1,2,false);
		
		cells[2][0] = new Cell(2,0,false);
		cells[2][1] = new Cell(2,1,false);
		cells[2][2] = new Cell(2,2,false);
		
		cells[1][1].setNumberOfAliveNeighbors(cells);
		
		assertFalse(cells[1][1].willRevive());
	}
	
	@Test
	public void cellShouldNotBeNeighbord() throws Exception {

		Cell cell1= new Cell(0,0, false);
		Cell cell2= new Cell(2,2, false);
		
		assertFalse(cell1.isNeighboard(cell2));
	} 
	
	@Test
	public void cellShouldNotBeNeighbord2() throws Exception {

		Cell cell1= new Cell(0,0, false);
		Cell cell2= new Cell(1,2, false);
		
		assertFalse(cell1.isNeighboard(cell2));
	} 
}

